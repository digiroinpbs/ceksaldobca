package id.corechain

import com.mashape.unirest.http.Unirest
import com.rethinkdb.RethinkDB
import com.rethinkdb.gen.exc.ReqlDriverError
import com.rethinkdb.gen.exc.ReqlError
import com.rethinkdb.gen.exc.ReqlPermissionError
import com.rethinkdb.net.Connection
import org.apache.commons.codec.digest.DigestUtils
import org.openqa.selenium.By
import org.openqa.selenium.UnhandledAlertException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.Select
import java.util.*
import redis.clients.jedis.Jedis
import redis.clients.jedis.exceptions.JedisConnectionException
import java.io.*
import java.util.concurrent.TimeUnit
import java.time.MonthDay
import java.time.Year


class WebDriver(){
    fun getWebDriver(): WebDriver? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        var driver: WebDriver
        System.setProperty("webdriver.chrome.driver", props.get("webdriver.chrome.driver").toString())
        driver = ChromeDriver()
        return driver
    }
    fun getDBName(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("db.name").toString()
    }
    fun getPostUrl(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("post.url").toString()
    }
    fun getRedisParam(): HashMap<String,String>? {
        val map:HashMap<String,String> = HashMap<String,String>()
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        map.put("ip",props.get("redis.name").toString())
        map.put("port",props.get("redis.port").toString())
        map.put("password",props.get("redis.password").toString())
        map.put("db",props.get("redis.db").toString())
        return map
    }

    fun directory(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("directory.download").toString()
    }
}

fun main(args: Array<String>) {
    var result:String
    result= checkSaldo("ADRYANMA8927","070116",args)
    return
}

fun checkSaldo(username:String, password:String, args: Array<String>):String{
    var redisParam:HashMap<String,String> = id.corechain.WebDriver().getRedisParam() as HashMap<String, String>
    val jedis = Jedis(redisParam.get("ip"), redisParam.get("port")!!.toInt())
    if(!redisParam.get("password").equals("")){
        jedis.auth(redisParam.get("password"))
    }
    jedis.select(redisParam.get("db")!!.toInt())
    var result =""
    val webDriver =id.corechain.WebDriver().getWebDriver()
    if(webDriver!=null){
        var error =false
        var alertError =false
        try {
            webDriver.get("https://ibank.klikbca.com/")
            var element = webDriver.findElement(By.name("value(user_id)"))
            element.sendKeys(username)
            element = webDriver.findElement(By.name("value(pswd)"))
            element.sendKeys(password)
            webDriver.findElement(By.name("value(Submit)")).click()

            webDriver.switchTo().defaultContent()
            var frame = webDriver.findElement(By.name("menu"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.linkText("Account Information")).click()
            jedis.set("BCA:lastLogin",Date().time.toString())
            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("menu"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.linkText("Balance Inquiry")).click()

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("atm"))
            webDriver.switchTo().frame(frame)
            var balance =webDriver.findElement(By.xpath("//tbody//tr//td//div[@align='right']")).text
            var saldo = balance.replace(".","+")
            saldo = saldo.replace(",",".")
            saldo = saldo.replace("+",",")

            var now =System.currentTimeMillis() / 1000L
            result=result+"{\"bank\":\"BCA\",\"amount\":\""+saldo+"\",\"lastupdate\":\""+now+"\"}"
            jedis.set(username+":BCA:"+now,result)
            jedis.set(username+":BCA:lastest",result)
            result ="result :["

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("menu"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.linkText("Account Statement")).click()

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("atm"))
            webDriver.switchTo().frame(frame)

            if(args.size>0){
                var drop:Select = Select(webDriver.findElement(By.xpath("//select[@name='value(startDt)']")))
                drop.selectByVisibleText(args[0])
                drop = Select(webDriver.findElement(By.xpath("//select[@name='value(startMt)']")))
                drop.selectByValue(args[1])
                drop = Select(webDriver.findElement(By.xpath("//select[@name='value(startYr)']")))
                drop.selectByVisibleText(args[2])
            }

            webDriver.findElement(By.xpath("//input[@name='value(submit2)']")).click()
            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("header"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.linkText("[ LOGOUT ]")).click()

            TimeUnit.SECONDS.sleep(5)
            var directory = id.corechain.WebDriver().directory()
            val folder = File(directory + "/")
            val listOfFiles = folder.listFiles()
            for (i in listOfFiles.indices) {
                if (listOfFiles[i].isFile()) {
                    if (listOfFiles[i].getName().contains(".CSV")) {
                        listOfFiles[i].renameTo(File(directory + "/mutasi/" + listOfFiles[i].getName() + ""))
                        listOfFiles[i].delete()
                        val csvFile = directory + "/mutasi/" + listOfFiles[i].getName() + ""
                        val cvsSplitBy = ","
                        var counter = 0
                        try {
                            BufferedReader(FileReader(csvFile)).use { br ->
                                var indicator = false
                                var test = true;
                                while (test) {
                                    counter++
                                    val line = br.readLine()
                                    if (counter == 6 || indicator == true) {
                                        var splittedLine = line.split(",")
                                        if (splittedLine[2].equals("NO TRANSACTION") || splittedLine[0].equals("Ending Balance")) {
                                            break
                                        } else if (splittedLine[0].equals("Starting Balance") || splittedLine[0].equals("Debet") || splittedLine[0].equals("Credit")) {
                                            continue
                                        } else {
                                            try {
                                                var res = ""
                                                indicator = true
                                                var splitDate = splittedLine[0].replace("'","").split("/")
                                                var date=""
                                                if(splitDate[0].equals("PEND ")){
                                                    date = ""+Year.now()+MonthDay.now()
                                                    date = date.replace("-","")
                                                }else{
                                                    date = ""+Year.now()+splitDate[1]+splitDate[0]
                                                }
                                                var type = ""
                                                var amount = splittedLine[3].replace(".00", "").toInt()
                                                res = res + "{\"Tanggal\":\"" + date + "\","
                                                res = res + "\"Keterangan Transaksi\":\"" + splittedLine[1] + "\","

                                                if (splittedLine[4].equals("CR")) {
                                                    type = "in"
                                                    res = res + "\"Debet\":\"" + 0 + "\","
                                                    res = res + "\"Kredit\":\"" + amount + "\"}"
                                                } else {
                                                    type = "out"
                                                    res = res + "\"Debet\":\"" + amount + "\","
                                                    res = res + "\"Kredit\":\"" + 0 + "\"}"
                                                }
//                                                val r: RethinkDB = RethinkDB.r;
//                                                val conn: Connection = r.connection().hostname("rth0.corechain.id").port(28015).db(id.corechain.WebDriver().getDBName()).user("digiro", "digiro").connect()
//                                                r.table("mutasi").insert(r.hashMap("id", sha1converter(res)).with("date", date.toInt()).with("desc", splittedLine[1]).with("type", type).with("amount", amount).with("processed", -1).with("bank", "BCA")
//                                                ).run<String>(conn)
//                                                conn.close()
//                                                if(jedis.get("bca:"+sha1converter(res)) == null){
//                                                    jedis.set("bca:"+sha1converter(res),sha1converter(res))
                                                    val postResponse = Unirest.post(id.corechain.WebDriver().getPostUrl())
                                                            .header("accept", "application/json")
                                                            .header("Content-Type", "application/json")
                                                            .body(res).asJson()
//                                                }
                                            } catch (e: IndexOutOfBoundsException) {
                                                e.printStackTrace()
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        break
                    }
                }
            }
        }catch (e:NullPointerException){
            print("Directory not found")
            error=true
        }catch (e:NoSuchElementException){
            print("Problem")
            error=true
        }catch (e:ReqlDriverError){
            System.out.println("connection rethink DB error")
            error=true
        }catch (e:JedisConnectionException){
            e.printStackTrace()
            System.out.println("connection redis error")
            error=true
        }catch (e:ReqlPermissionError){
            System.out.println("connection rethink DB permission Error")
            error=true
        }catch (e: ReqlError){
            System.out.println("rethink DB problem")
            error=true
        }catch (e:UnhandledAlertException){
            print("alert exception")
            alertError=true
            error=true
        }catch (e:Exception){
            e.printStackTrace()
            error=true
        }finally {
            if(alertError){
                val alert = webDriver.switchTo().alert()
                alert.dismiss()
            }
            if(error){
                webDriver.switchTo().defaultContent()
                var frame = webDriver.findElement(By.name("header"))
                webDriver.switchTo().frame(frame)
                webDriver.findElement(By.linkText("[ LOGOUT ]")).click()
                TimeUnit.SECONDS.sleep(5)
            }
            jedis.quit()
            webDriver.quit()
        }
    }
    return result;
}

fun sha1converter(param: String): String {
    return DigestUtils.sha1Hex(param)
}
